$script:oldtabexpansion = Get-Item Function:\TabExpansion -ErrorAction SilentlyContinue
$script:expansions = @()

<#
.Synopsis
   Short description
.DESCRIPTION
   Long description
.EXAMPLE
   Example of how to use this cmdlet
.EXAMPLE
   Another example of how to use this cmdlet
#>
function Set-TabExpansionAlias
{
    [CmdletBinding()]
    Param
    (
        # Param1 help description
        [Parameter(Mandatory=$true,
                   ValueFromPipelineByPropertyName=$true,
                   Position=0)]
        [string]$Name,

        # Param2 help description
        [Parameter(Mandatory=$true,
                   ValueFromPipelineByPropertyName=$true,
                   Position=1)]
        [string]$Value,

        [switch]$Force,
        [switch]$PassThru
    )

    Process {
        $script:expansions | % { Write-Verbose $_.Name }
        $found = $script:expansions | Where-Object { $_.Name -eq $Name }
        if ($found) {
            if (-not $Force) {
                throw "'$Name' is already set."
            }
            $found.Value = $Value
        } else {
            $found = New-TabExpansionAlias -Name:$Name -Value:$Value
            $script:expansions += @($found)
        }
        if ($PassThru) {
            $found
        }
        $script:expansions | % { Write-Verbose $_.Name }
    }
}

<#
.Synopsis
   Short description
.DESCRIPTION
   Long description
.EXAMPLE
   Example of how to use this cmdlet
.EXAMPLE
   Another example of how to use this cmdlet
#>
function Get-TabExpansionAlias
{
    [CmdletBinding()]
    Param
    (
        # Param1 help description
        [Parameter(Mandatory=$False,
                   ValueFromPipelineByPropertyName=$true,
                   Position=0)]
        [string[]]$Name = '*',

        # Param2 help description
        [Parameter(Mandatory=$false,
                   Position=1)]
        [string[]]$Exclude
    )

    Process
    {
        $script:expansions | % { Write-Verbose $_.Name }
        $script:expansions |
            Where-Object {
                $alias = $_
                [bool]($Name | Where-Object { $alias.Name -like $_ })
            } |
            Where-Object {
                $alias = $_
                [bool](-not ($Exclude | Where-Object { $alias.Name -like $_ }))
            } |
            Sort-Object Name
    }
}

<#
.Synopsis
   Short description
.DESCRIPTION
   Long description
.EXAMPLE
   Example of how to use this cmdlet
.EXAMPLE
   Another example of how to use this cmdlet
#>
function Remove-TabExpansionAlias
{
    [CmdletBinding()]
    Param
    (
        # Param1 help description
        [Parameter(Mandatory=$true,
                   ValueFromPipelineByPropertyName=$true,
                   Position=0)]
        [string[]]$Name
    )

    Process
    {
        $script:expansions = $script:expansions | Where-Object { @($Name) -notcontains $_.Name }
    }
}

function New-TabExpansionAlias
{
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory=$true,
                   Position=0)]
        [string]$Name,

        [Parameter(Mandatory=$true,
                   Position=1)]
        [string]$Value
    )

    New-Object PSObject -Property @{
        Name = $Name
        Value = $Value
    } | ForEach-Object {
        $defaultProperties = @('Name','Value')
        $defaultDisplayPropertySet = New-Object System.Management.Automation.PSPropertySet('defaultDisplayPropertySet',[string[]]$defaultProperties)
        $_ | Add-Member MemberSet PSStandardMembers ([System.Management.Automation.PSMemberInfo[]]@($defaultDisplayPropertySet))
        $_.PSObject.TypeNames.Insert(0, 'TabExpansions.TabExpansionAlias')
        $_
    }
}

function tabexpansion($line, $word) {
    if ($word[0] -eq '!') {
        Get-TabExpansionAlias ($word.Substring(1) + '*') | ForEach-Object { $_.Value }
    } elseif ($script:oldtabexpansion) {
        & $script:oldtabexpansion $PSBoundParameters
    }
}

Export-ModuleMember -Function Set-TabExpansionAlias,Get-TabExpansionAlias,Remove-TabExpansionAlias,tabexpansion